# JKMonitors - Signage for a Raspberry PI

To update or install JKMonitors:

```
$ bash <(curl -sL https://cdn.justinkruit.me/jkmonitors.sh)
```

(The installation will take 15-20 minutes or so depending on your connectivity and the speed of your SD card.)
